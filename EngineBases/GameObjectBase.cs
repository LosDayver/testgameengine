﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EngineApi;
using System.Drawing;

namespace EngineBases
{
    public abstract class GameObjectBase : IGameObject
    {
        public abstract int XPosition { get; set; }
        public abstract int YPosition { get; set; }
        public abstract Bitmap[] Animation { get; set; }
        public abstract int AnimationCurrentFrame { get; set; }
        public abstract IRoom Room { get; set; }

        public GameObjectBase() { }
        public abstract void CreateGameObject();
        public abstract void DestroyGameObject();
        public abstract void EveryTick();
        public abstract void OnCreate();
        public abstract void OnDestroy();
    }
}
