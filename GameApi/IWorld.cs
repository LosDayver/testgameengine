﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineApi
{
    public interface IWorld
    {
        //Свойства
        IRoom[,] GameRooms();
        IRoom[] FunctionalRooms();
    }
}
