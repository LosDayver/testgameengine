﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace EngineApi
{
    public interface IGameObject
    {
        //Свойства
        int XPosition { get; set; }
        int YPosition { get; set; }
        Bitmap[] Animation { get; set; }
        int AnimationCurrentFrame { get; set; }
        IRoom Room { get; set; }

        //События
        void OnCreate();
        void EveryTick();
        void OnDestroy();

        //Методы
        void CreateGameObject();
        void DestroyGameObject();
    }
}
